# LS-DYNA - umat TTC cohesive
This is an adaption of the material model presented by [Catalanotti et al.](https://doi.org/10.1016/j.compstruct.2017.09.014), implemented as Fortran subroutine for usage as LS-DYNA `*MAT_USER_DEFINED_MATERIAL_MODELS` with an explicit time integration scheme. The code in this repository is based on the initial implementation by [Kellner](https://www.researchgate.net/publication/327623424_How_To_-_user_defined_material_models_with_LS-Dyna_on_Windows). The modified version will be presented at the [14th European LS-DYNA Conference 2023](https://www.dynamore.de/en/training/conferences/upcoming/14th-european-ls-dyna-conference).

## Overview

The principal functionality of the subroutine is illustrated below.

![Simplified flowchart of UMAT functionality](imgs/umat_flowchart.png)

 1. First, the adaption parameters $\alpha_R$ and $\alpha_{\mathcal{G}}$ are calculated in case of through thickness compression (TTC), otherwise they are set to $1$, indicating no changes to the original behaviour.
 2. Next, they are applied to strength and critical energy release rate respectively and the dependent variables $\delta_{II0}$ and $u_td$ are updated accordingly. 
 3. In the following steps, the loading scenario is assessed and in case of through thickness tension (TTT) the influence of mode-mixity $\beta$ is taken into account. For numerical stability, a mathematically equivalent but rearranged formulation has been chosen for the implementation. The modifications prevent numerical instabilities in edge cases such as $0 < \delta_3 \ll 1$, leading to very large values for $\beta$. 
 4. With the adapted material parameters and determined loading conditions, the element is checked for final failure and crack initiation. In the former case the element is immediately deleted, in the latter a linear damage evolution is applied, leading to an isotropic reduction of the material’s stiffness. 
 5. Finally, the stress responses are calculated based on the given separations and the resulting material state.

## Usage

This explanation assumes a folder structure under windows:
```
src
|---umat_TTC_coh.F
|---ls-dyna_mpp*
    |---nmake.exe
    |---makefile
    |---dyn21umatc.F
```

1. In `dyn21umatc.F` replace one of the calls to `umat41c`-`umat50c` by `umatCohTTC`, with identical call signature.
2. In `makefile` add `umat_TTC_coh.obj` to the `OBJS` list
3. Add `umat_TTC_coh` to compilation and linking:
    ```makefile
    # Existing
    .F.obj:
        $(FC) $(FFLAGS) $*.F
    # To be added
    umat_TTC_coh.obj: ../umat_TTC_coh.F
        $(FC) $(FFLAGS) umat_TTC_coh.F
    ```
4. Launch a terminal in `ls-dyna_mpp*` with Fortran compiler enabled (i.e. Intel Parallel Studio). To first delete the old exe and compile the new one, run
    ```shell
    del mppdyna.exe & nmake
    ```
5. The created `mppdyna.exe` can be used like normal LS-DYNA executables. In order to use the added material model, set `Mt` (3rd parameter of 1st Card in `*MAT_USER_DEFINED_MATERIAL_MODELS`) to the number you replaced in `dyn21umatc.F`. I.e., if you replaced `umat50c`, the beginning of the material card would look like this:
    ```k
    *MAT_USER_DEFINED_MATERIAL_MODELS
    $#     mid       rho        Mt   
            xx        yy        50   
    ```