!***********************************************************************
      subroutine umatCohTTC(idpart,cm,lft,llt,fc,dx,dxdt,aux,ek,
     1 ifail,dt1siz,crv,nnpcrv,nhxbwp,cma,maketan,dsave,ctmp,elsiz,
     2 reject,ip,nip)
!
! vectorized cohesive material user model example
!
!*** variables
! idpart ---- part ID
! cm -------- material constants
! lft,llt --- start and end of block
! fc -------- components of the cohesive force
! dx -------- components of the displacement
! dxdt ------ components of the velocity
! aux ------- history storage
! in LS-Prepost: aux(i,1)=effective plastic strain,
! aux(i,2)=history var#1,
! aux(i,3)=history var#2, ...
! ek -------- max. stiffness/area for time step calculation
! ifail ----- =.false. not failed
! =.true. failed
! dt1siz ---- time step size
! crv ------- curve array
! nnpcrv ---- # points per curve for crv array
! nhxbwp ---- internal element id array, lqfinv(nhxbwp(i),2)
! gives external element id
! cma ------- additional memory for material data defined by
! LMCA in 2nd card, 6th field of *MAT_USER_DEFINED
! maketan --- true for implicit
! dsave ----- material stiffness array, define for implicit
! ctmp ------ current temperature
! elsiz ----- characteristic element size (=sqrt(area))
! reject ---- set to .true. if this implicit iterate is
! to be rejected for some reason (implicit only)
! ip -------- integration point number
! nip ------- total number of integration points
!
!*** dx, dxdt, and fc are in the local coordinate system:
! components 1 and 2 are in the plane of the cohesive surface
! component 3 is normal to the plane
!
!*** cm storage convention
! (1) =0 density is per area
! =1 density is per volume
! (2) number of integration points for element deletion
! =0 no deletion
! (3:48) material model constants
!
! User defined cohesive model. Last worked on 06.2023
! Increased s and giic due to through thickness compression (TTC)
! Moritz Kuhtz, Moritz.Kuhtz@tu-dresden.de
! Johannes Gerritzen, johannes.gerritzen@tu-dresden.de
! Institute for Lightweight Engineering and Polymer Technologies
! Technische Universität Dresden
!
! SOURCE: https://doi.org/10.1016/j.compstruct.2017.09.014
!
! subroutine bases on the work of Leon Kellner (see below)
! changes are highlighted by "NEW" tag
!
! User defined cohesive model. Last worked on 11.2018.
! Leon Kellner, leon.kellner@tuhh.de
! Source: Dávila, C.; Camanho, P. (2001): Decohesion Elements using Two and
! Three-Parameter Mixed Mode Criteria. In: American Helicopter Society
!onference.
! Williamsburg, VA, 2001.
!
!
      include 'nlqparm'
      INCLUDE 'bk06.inc' ! needed for ncycle
      INCLUDE 'iounits.inc' ! io variables
!
      common/aux33loc/ix1(nlq),ix2(nlq),ix3(nlq),ix4(nlq),ix5(nlq),
     1  ix6(nlq),ix7(nlq),ix8(nlq),mxt(nlq)
      common/bk28/summss,xke,xpe,tt,xte0,erodeke,erodeie,selie,selke,
     1  erodehg,eintcnt,engjnt
!
!_TASKCOMMON (aux33loc)
!
! *** Variables ---------------------------------------------------------------!
! *** Dyna variables / internal
      logical ifail,maketan,reject,debugging
! declare arrays, * = assumed size array
      dimension cm(*),fc(nlq,*),dx(nlq,*),dxdt(nlq,*),
     1  aux(nlq,*),ek(*),ifail(*),dt1siz(*),crv(lq1,2,*),
     2  nhxbwp(*),cma(*),dsave(nlq,6,*),ctmp(*),elsiz(*)
      integer nnpcrv(*)
      INTEGER idpart !Leon: INTEGER8 idpart
! *** user variables / external
      ! separation in tangential mode (mode II)
      ! mixed mode onset of softening
      ! ultimate mixed mode displacement(failure)
      real, dimension(nlq) :: deltaII, deltaM, delta0, beta, deltaF,
     1  deltaFZ, deltaFN, deltaFG, deltaFGI, deltaFGII, deltaFGXMU,
     2  sfS, sfGiic, delta0II, utd, dmg
      real delta0I ! damage initiation displacement for mode I
      real ePen ! penetration stiffness
      integer hsvs_limit !number of history variables
      integer terminate
      real en, et, t, s, GIC, GIIC, xmu, und, penalty, etaS, etaGiic
! debuggging mode
      debugging=.false.
!
! *** cm(*) material constants storage convention
! (1) = 0 density is per area
! = 1 density is per volume
! (2) number of integration points for element deletion
! = 0 no deletion
      en=cm(3) ! EN - tangential stiffness (units of stress/length)
      et=cm(4) ! ET - normal stiffness (units of stress/length)
      t=cm(5) ! T - Peak traction in normal direction (stress units)
      s=cm(6) ! S - Peak traction in tangential direction (stress units)
      GIC=cm(7) ! GIC - Energy release rate for mode I (units of stress*length)
      GIIC=cm(8) ! GIIC - Energy release rate for mode II (units of stress*length)
      xmu=cm(9) ! XMU - Exponent of the mixed mode criteria
      penalty=cm(10)! - stiffness multiplier, is ignored if <= 1
      etaS=cm(11) ! NEW: factor for s due to normal compressive stress
      etaGiic=cm(12) ! NEW: factor for giic due to normal compressive stress
!
! *** history variables, aux(nlq,*)
! (1) ! deltaMaxM - maximum value of displacement
! (2) ! dmg - damage value
! (3) ! peak stress reached
! (4) ! fail reached
! (5) ! sfS
! (6) ! sfGiic
! (7) ! beta
      hsvs_limit=7
! *** beginning of routine --------------------------------------------!
      ! input checks
      ! taken from LS-Dyna Material Models Manual 2017, MAT_138 description
      IF (((2*gic)/(t*t/en)) <=1) then
        write (iotty,*) "Error: peak stress is past failure points!"
        write (iotty,*) "Check input data for normal mode!"
        stop
      elseif (((2*giic)/(s*s/et)) <=1) then
        write (iotty,*) "Error: peak stress is past failure points!"
        write (iotty,*) "Check input data for tangential mode!"
        stop
      endif
!
      IF (penalty >= 1) then
        ePen=penalty*max(en,et) ! penetration stiffness
      endif
!
      delta0I=t/en ! damage initiation displacements (fixed values)
      und=gic*2/t ! initial ultimate displacements
      ! delta0II=s/et ! Calculation per integration point
      ! utd=giic*2/s ! Calculation per integration point
!
! *** set beta to zero, only for plot reasons in normal tension mode
      beta(:) = 0.

      do i=lft,llt
!------NEW---TTC-adaption----------------------------------------------!
! *** if condition to check if normal stress is compression
! *** then scale s and giic by factors sfS and sfGiic
        if (aux(i,3).eq.0) then
          if (fc(i,3)<0) then
            sfS(i)=(1-etaS*fc(i,3))**0.5
            sfGiic(i)=1-etaGiic*fc(i,3)
          else
            sfS(i)=1
            sfGiic(i)=1
          endif
! *** save scale factors to history variables
          aux(i,5) = sfS(i)
          aux(i,6) = sfGiic(i)
        else
! *** once peak stress has reached last updated scale factors are used
          sfS(i) = aux(i,5)
          sfGiic(i) = aux(i,6)
        endif
!
        delta0II(i) = (sfS(i) * s) / et
        utd(i) = (sfGiic(i) * giic) * 2 / (sfS(i) * s)
!------END NEW---------------------------------------------------------!
!
!-------Mode-Adaption--------------------------------------------------!
        ! separation in tangential mode (mode II)
        deltaII(i) = sqrt(dx(i,1)**2+dx(i,2)**2)
!
        ! total mixed mode separation;
        ! only calculated if normal displacement > 0
        if (dx(i,3) > 0) then
          deltaM(i) = sqrt(deltaII(i)**2+dx(i,3)**2)
        else ! mixed mode displacement = tangential displacement
          deltaM(i) = deltaII(i)
        endif
!
        if ((dx(i,3) > 0)) then ! mixed mode displacement
!
          beta(i) = deltaII(i)/dx(i,3) ! mode mixity
!
!---NEW----Robust-implementation---------------------------------------!
          ! mixed mode damage initiation displacement
          delta0(i) = delta0I*delta0II(i)*sqrt((1+beta(i)**2.) / (
     1      delta0II(i)**2.+beta(i)**2.*delta0I**2.
     2    ))
          deltaF(i) = 2./delta0(i) * (
     1      1./((en/((1+beta(i)**2.)*gic))**xmu + (
     2        (1. - 1. / (beta(i)**2. + 1.))
     3        *et/(sfGiic(i) * giic)
     4      )**xmu)
     5    )**(1/xmu)
!
        elseif ((dx(i,3) <= 0)) then ! only tangential displacement
          delta0(i) = delta0II(i) ! set delta0 to mode II delta0
          deltaF(i) = utd(i) ! set deltaF to mode II deltaF
        endif
        aux(i,7) = beta(i) ! write beta to history variable
!
!------END NEW---------------------------------------------------------!
        ! failure if mixed mode displacement > deltaF
        ifail(i) = (deltaM(i) > deltaF(i))
        if (Ifail(i)) then
          aux(i,4) = 1.
        else
          aux(i,4) = 0.
        endif
!
!-----------Damage-detection-&-stress-update---------------------------!
        ! identify if peak stress has been reached
        ! aux(i,3) = 0 ! default case: no damage ! kein zurück mehr
        if (deltaM(i) >= delta0(i)) then
          aux(i,3) = 1 ! peak stress has been reached / softening initiated
        endif
!
        aux(i,1) = max(deltaM(i), aux(i,1)) ! save maximum displacement value
        ! update cohesive forces / stresses and stiffness
        ! ***no damage / softening
        if ((deltaM(i) <= delta0(i)) .and. (aux(i,2) <= 0)) then
          fc(i,1)=et*dx(i,1) ! cohesive forces/stresses through displacement and stiffness
          fc(i,2)=et*dx(i,2)
          fc(i,3)=en*dx(i,3)
          ek(i)=max(et,en) ! max stiffness
!
          ! ***damage / softening, but not failed yet
        elseif ((deltaM(i) > delta0(i) .or. (aux(i,2) > 0)) .and.
     1   (.not. ifail(i))) then
          dmg(i) = (deltaF(i) / aux(i,1))*(
     1      (aux(i,1)-delta0(i)) / (deltaF(i)-delta0(i))
     2    ) ! update damage variable
          aux(i,2) = max(dmg(i), aux(i,2))
          fc(i,1)=et*(1-aux(i,2))*dx(i,1) ! cohesive forces/stresses with updated stiffness
          fc(i,2)=et*(1-aux(i,2))*dx(i,2)
          fc(i,3)=en*(1-aux(i,2))*dx(i,3)
          ek(i)=max(et*(1-aux(i,2)),en*(1-aux(i,2))) ! max stiffness
!
          ! ***complete damage / failure
        elseif (ifail(i)) then
          aux(i,2)=1
          fc(i,1)=0
          fc(i,2)=0
          fc(i,3)=0
        endif
      enddo
!
!
! !State and History output
      IF (debugging) THEN
        DO i=lft,llt
          IF (tt.gt.1.45e-3)THEN
            IF (lqfinv(nhxbwp(i),2).eq.36812) THEN
              ! !             !IF((idelev(i).eq.2701))THEN
              IF (ip.eq.1) THEN
                OPEN(111,file='debuglogv1.csv',recl=600)
                WRITE(111,122) ncycle,ip, tt,
     1           fc(i,1),fc(i,2),fc(i,3),
     2           dx(i,1),dx(i,2),dx(i,3),
     3           aux(i,1),delta0(i),deltaF(i),
     4           aux(i,2),aux(i,3),aux(i,4),
     5           aux(i,5),aux(i,6),aux(i,7)
              ENDIF
              IF (ip.eq.2) THEN
                OPEN(112,file='debuglogv2.csv',recl=600)
                WRITE(112,122) ncycle,ip, tt,
     1           fc(i,1),fc(i,2),fc(i,3),
     2           dx(i,1),dx(i,2),dx(i,3),
     3           aux(i,1),delta0(i),deltaF(i),
     4           aux(i,2),aux(i,3),aux(i,4),
     5           aux(i,5),aux(i,6),aux(i,7)
              ENDIF
              IF (ip.eq.3) THEN
                OPEN(113,file='debuglogv3.csv',recl=600)
                WRITE(113,122) ncycle,ip, tt,
     1           fc(i,1),fc(i,2),fc(i,3),
     2           dx(i,1),dx(i,2),dx(i,3),
     3           aux(i,1),delta0(i),deltaF(i),
     4           aux(i,2),aux(i,3),aux(i,4),
     5           aux(i,5),aux(i,6),aux(i,7)
              ENDIF
              IF (ip.eq.4) THEN
                OPEN(114,file='debuglogv4.csv',recl=600)
                WRITE(114,122) ncycle,ip, tt,
     1           fc(i,1),fc(i,2),fc(i,3),
     2           dx(i,1),dx(i,2),dx(i,3),
     3           aux(i,1),delta0(i),deltaF(i),
     4           aux(i,2),aux(i,3),aux(i,4),
     5           aux(i,5),aux(i,6),aux(i,7)
              ENDIF
            ENDIF
          ENDIF
        ENDDO
      ENDIF
      !  CLOSE(110)
!  111  format(F20.10,',')
!  112  format(I10,',')
!  113  format(I10,',',I10,',',I10,',',I10,',')
!  110  format(I10,',',F20.10,',',
!      & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,','
!      & ,F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10)
!  121  format(I10,',',I10,',',F20.10,',',
!      & F20.10,',',F20.10,',',F20.10,',',F20.10,',')
!
  122 format(I10,',',I10,',',F20.10,',',
     1 F20.10,',',F20.10,',',F20.10,',',
     2 F20.10,',',F20.10,',',F20.10,',',
     3 F20.10,',',F20.10,',',F20.10,',',
     4 F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10)

  151 format(E20.10,',',I10,',',I10,',',
     1 E20.10,',',E20.10,',',E20.10)
      ! ,',',E20.10,',',
      !  2 E20.10,',',E20.10,',',E20.10,',',
      !  3 E20.10,',',E20.10,',',E20.10)
!
      ! NaN detector
!
      ! DO i=lft,llt
      !  terminate=0
      !  IF(isnan(fc(i,1)))terminate=1
      !  IF(isnan(fc(i,2)))terminate=1
      !  IF(isnan(fc(i,3)))terminate=1
      !  DO k=1,hsvs_limit
      !    IF(isnan(aux(i,k)))terminate=1
      !  ENDDO
      !  IF(terminate.eq.1)THEN
      !    OPEN(113,file='debuglogNAN.txt')
      !   !  WRITE(113,*)ncycle,lqfinv(nhxbwp(i),2),lft,llt
      !    WRITE(113,*)ncycle,i,lft,llt
      !  ENDIF
      ! ENDDO
!
!
! DO i=lft,llt
! IF(idelev(i).eq.INT(cm(11)))THEN
! OPEN(110,file='debuglogv1.txt')
! OPEN(120,file='debuglogv2.txt')c
!
!
! IF(ncycle.EQ.0)THEN
! write(110,111)
! write(120,121, advance="no")i
! DO l=1,hsvs_limit
! write(120,123, advance="no")l
! ENDDO
! ENDIF
! IF(ncycle.GE.INT(cm(10)))THEN
! k=1
! write(110,112)ncycle,tt,
! & eps(i,1),eps(i,2),eps(i,3),eps(i,4),eps(i,5),eps(i,6),
! & sig1(i),sig2(i),sig3(i),sig4(i),sig5(i),sig6(i),
! & ef(k),ek(k),em(k),rf(k),rk(k),rm(k),
! & w(k,1),wc(k,1),w(k,2),wc(k,2),w(k,3)
! write(120,121, advance="no")ncycle
! DO l=1,hsvs_limit
! write(120,122, advance="no")hsvs(i,l)
! ENDDO
! !write(120,*)" "
! ENDIF
!
! ENDIF
! ENDDO
!
! 111 FORMAT("    ncycle,                time,                eps1,",
! &"                eps2,               eps3,                eps4,",
! &"                eps5,             ",
! &"  eps6,                sig1,                sig2,              ",
! &"     sig3,                sig4,                sig5,           ",
! &"     sig6,                  ef,                  ek,           ",
! &"     em,                  rf,                  rk,             ",
! &"     rm,                  w1,           ",
! &"    wc1,                  w2,                 wc2,             ",
! &"     w3")
!
! 112 format(I10,',',F20.10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10)
! 121 FORMAT(I10,',')
! 122 FORMAT(F20.10,',')
! 123 FORMAT(I20,',')
!
! DO i=lft,llt
! k=1
! write(110,110)ncycle,
! & eps(i,1),sig1(i),f1c(1,i),f2t(1,i),f2c(1,i),
! & d1p(1,i),d1m(1,i),d2p(1,i),d2m(1,i)
!
! 110 format(I10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10,',',F20.10,',',
! & F20.10,',',F20.10,',',F20.10,',',F20.10)
! ENDDO
!
      return
      end subroutine umatCohTTC
